const spawnEnemy = ({
  spriteName = "orc",
  maxHealth = 20,
  speed = 100,
} = {}) => {
  const enemy = map.spawn([
    sprite(spriteName),
    pos(...startPoint),
    anchor("center"),
    agent({ speed, allowDiagonals: false }),
    area(),
    health(maxHealth),
    "enemy",
  ]);

  enemy.add([
    rect(20, 2),
    color(255, 0, 0),
    pos(-10, 10),
    "lifebar",
  ]);

  enemy.add([
    rect(20, 2),
    color(0, 255, 0),
    pos(-10, 10),
    "health",
  ]);

  enemy.on("hurt", () => {
    enemy.children
      .find((child) => child.is("health"))
      .width = (enemy.hp() / maxHealth) * 20;
  });

  enemy.on("death", () => {
    enemy.destroy();
    addKaboom(enemy.pos, { scale: 0.1 });
  });

  // PATHFIND
  enemy.setTarget(vec2(...endPoint));
  enemy.onCollide("dispawArea", () => {
    enemy.destroy();
  })

  // ANIMATION
  enemy.play("walk-bottom");
  enemy.onUpdate(() => {
    const velocity = enemy.pos.sub(enemy.getNextLocation());

    if (Math.round(velocity.x) < 0 && enemy.curAnim() !== "walk-right") {
      enemy.play("walk-right", velocity);
    } else if (Math.round(velocity.x) > 0 && enemy.curAnim() !== "walk-left") {
      enemy.play("walk-left");
    } else if (Math.round(velocity.y) < 0 && enemy.curAnim() !== "walk-bottom") {
      enemy.play("walk-bottom");
    } else if (Math.round(velocity.y) > 0 && enemy.curAnim() !== "walk-top") {
      enemy.play("walk-top");
    }
  });

  return enemy;
}
