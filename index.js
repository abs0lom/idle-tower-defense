const TILE_SIZE = 16;
const SCALE = 2;

kaboom({
  scale: SCALE,
});

const mousePosToTile = (position) => {
  const x = Math.floor(position.x / TILE_SIZE / SCALE) * TILE_SIZE;
  const y = Math.floor(position.y / TILE_SIZE / SCALE) * TILE_SIZE;
  return { x, y };
}

loadSpriteAtlas("assets/MiniWorldSprites/AllAssetsPreview.png", {
  "grass": {
    x: TILE_SIZE * 11,
    y: TILE_SIZE * 15,
    width: TILE_SIZE,
    height: TILE_SIZE,
  },
  "sand": {
    x: TILE_SIZE * 11,
    y: TILE_SIZE * 16,
    width: TILE_SIZE,
    height: TILE_SIZE,
  },
  "tower": {
    x: TILE_SIZE * 3,
    y: TILE_SIZE * 12,
    width: TILE_SIZE,
    height: TILE_SIZE,
  },
  "wood-tower": {
    x: TILE_SIZE * 0,
    y: TILE_SIZE * 12,
    width: TILE_SIZE,
    height: TILE_SIZE,
  },
  "orc": {
    x: TILE_SIZE * 48,
    y: TILE_SIZE * 0,
    width: TILE_SIZE * 5,
    height: TILE_SIZE * 4,
    sliceX: 5,
    sliceY: 4,
    anims: {
      "walk-bottom": { from: 2,  to: 4,  pingpong: true,  loop: true, speed: 5 },
      "walk-top":    { from: 7,  to: 9,  pingpong: true,  loop: true, speed: 5 },
      "walk-right":  { from: 12, to: 14, pingpong: true,  loop: true, speed: 5 },
      "walk-left":   { from: 17, to: 19, pingpong: true,  loop: true, speed: 5 },
    },
  },
  "green-slime": {
    x: TILE_SIZE * 43,
    y: TILE_SIZE * 14,
    width: TILE_SIZE * 6,
    height: TILE_SIZE * 4,
    sliceX: 6,
    sliceY: 4,
    anims: {
      "walk-bottom": { from: 2,  to: 5,  pingpong: true,  loop: true, speed: 5 },
      "walk-left":   { from: 8,  to: 11,  pingpong: true,  loop: true, speed: 5 },
      "walk-right":  { from: 14, to: 17, pingpong: true,  loop: true, speed: 5 },
      "walk-top":     { from: 20, to: 23, pingpong: true,  loop: true, speed: 5 },
    },
  },
})

loadSprite("box-selector", "assets/MiniWorldSprites/User Interface/BoxSelector.png", {
  sliceX: 2,
  anims: {
    idle: { from: 0, to: 1, loop: true, speed: 2 }
  }
});

loadSpriteAtlas("assets/MiniWorldSprites/User Interface/Highlighted-Boxes.png", {
  "white-highlight": {
    x: TILE_SIZE * 0,
    y: TILE_SIZE * 0,
    width: TILE_SIZE,
    height: TILE_SIZE,
  },
  "green-highlight": {
    x: TILE_SIZE * 1,
    y: TILE_SIZE * 0,
    width: TILE_SIZE,
    height: TILE_SIZE,
  },
  "blue-highlight": {
    x: TILE_SIZE * 2,
    y: TILE_SIZE * 0,
    width: TILE_SIZE,
    height: TILE_SIZE,
  },
  "red-highlight": {
    x: TILE_SIZE * 3,
    y: TILE_SIZE * 0,
    width: TILE_SIZE,
    height: TILE_SIZE,
  },
  "yellow-highlight": {
    x: TILE_SIZE * 4,
    y: TILE_SIZE * 0,
    width: TILE_SIZE,
    height: TILE_SIZE,
  },
});

// LEVEL

const level = [
  "    #                       ",
  "    #                       ",
  "    #                       ",
  "    #                       ",
  "    #                       ",
  "    #                       ",
  "    #############           ",
  "                #           ",
  "                #           ",
  "  #######       #           ",
  "  #     #       #           ",
  "  #     #########           ",
  "  #                         ",
  "  #                         ",
  "  #                         ",
  "  ###############           ",
  "                #           ",
  "                #           ",
  "                #           ",
  "                #           ",
  "                #           ",
];

const startPoint = [
  (level[0].indexOf("#") * TILE_SIZE) + (TILE_SIZE / 2),
  TILE_SIZE / 2,
];

const endPoint = [
  (level[level.length - 1].indexOf("#") * TILE_SIZE) + (TILE_SIZE / 2),
  (level.length * TILE_SIZE) - 1,
];

const map = addLevel(level, {
  tileWidth: TILE_SIZE,
  tileHeight: TILE_SIZE,
  tiles: {
    " ": () => [
      sprite("grass"),
      tile({ isObstacle: true }),
    ],
    "#": () => [
      sprite("sand"),
      tile(),
      "path",
    ],
//    "F": () => [
//      sprite("tower"),
//      color(165,42,42),
//    ],
//    "T": () => [
//      sprite("tower"),
//      color(169, 150, 146),
//    ],
//    "B": () => [
//      sprite("wood-tower"),
//    ],
//    "M": () => [
//      sprite("tower"),
//    ],
//    "E": () => [
//      sprite("tower"),
//      color(100, 100, 200),
//    ],
  }
});

// TOWERS
const range = 1;

const spawnTower = (position) => {
  const isNotAvailable = []
    .concat(map.get("path"))
    .concat(map.get("tower"))
    .some((gameObject) => testRectPoint(gameObject, position.scale(1/SCALE)));

  if(isNotAvailable) {
    console.error("tile not available");
    return;
  }

  const { x, y } = mousePosToTile(position);
  const tower = map.spawn([
    sprite("tower"),
    color(100, 100, 200),
    pos(x, y),
    {
      range,
      damages: 1,
      cooldown: 10,
      counter: 0,
    },
    "tower",
  ])
  const towerArea = map.spawn([
    circle((range * TILE_SIZE) + (TILE_SIZE / 2)),
    opacity(0),
    follow(tower, vec2(TILE_SIZE / 2, TILE_SIZE / 2)),
    anchor("center"),
    area(),
  ])

  towerArea.onUpdate(() => {
    towerArea
      .getCollisions()
      .filter(({ target }) => target.is("enemy"))
      .forEach(({ target }) => {
        if(tower.counter === 0) {
          target.hurt(tower.damages);
          tower.counter = tower.cooldown;
        }
      });
    if (tower.counter !== 0) {
      tower.counter--;
    }
  });
}

// SELECTOR

const selector = map.spawn([
  sprite("box-selector"),
  tile(),
  z(100),
]);

selector.play("idle");

onMouseMove((position) => {
  const { x, y } = mousePosToTile(position);
  selector.moveTo(x, y);
});

onClick((button) => {
  if (button == "left") {
    spawnTower(mousePos());
  }
})

// GHOST TOWER

const ghostTower = map.spawn([
  sprite("tower"),
  color(100, 100, 200),
  opacity(0.7),
  follow(selector),
])

const rangeCircle = map.spawn([
  circle((range * TILE_SIZE) + (TILE_SIZE / 2)),
  color(0, 255, 0),
  opacity(0.3),
  follow(ghostTower, vec2(TILE_SIZE / 2, TILE_SIZE / 2)),
])

// ENEMIES
const dispawArea = map.spawn([
  circle(1),
  color(255, 0, 0),
  opacity(0.3),
  pos(...endPoint),
  area(),
  "dispawArea",
])

const enemies = [
  { maxHealth: 10, speed: 50, spriteName: "green-slime" },
  { maxHealth: 10, speed: 200 },
  { maxHealth: 50, speed: 100 },
  { maxHealth: 100, speed: 50 },
];

spawnEnemy();
setInterval(() => {
  const random = randi(enemies.length - 1);
  spawnEnemy(enemies[random]);
}, 5000);
